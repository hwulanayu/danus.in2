package com.danusin.registration.vo;

public class AuthVo {
    private String authString;

    public String getAuthString() {
        return authString;
    }

    public void setAuthString(String authString) {
        this.authString = authString;
    }
}
