package com.danusin.registration.service;

import com.danusin.registration.domain.persistence.User;
import com.danusin.registration.vo.AuthVo;
import com.danusin.registration.vo.ResponseVo;
import com.danusin.registration.vo.UserVo;

import java.util.Collection;

public interface UserService {
    User createUser(User user);
    ResponseVo createUser(UserVo vo);
    Collection<User> getListUsers();
    User findByUsername(String username);
    UserVo isAuth(String authString);
    AuthVo credential(String username);
    AuthVo restLogin(String username, String password);
    ResponseVo restLogout(String authString);
}
