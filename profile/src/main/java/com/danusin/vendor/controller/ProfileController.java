package com.danusin.vendor.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class ProfileController {
    @GetMapping("/profile")
    public String greeting(Model model) {
        model.addAttribute("user", "user");
        return "profile";
    }
}
