package com.danusin.search_danus;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SearchDanusApplication.class)
public class SearchDanusApplicationTests {

	@Test
	public void contextLoads() {
	}

}
