package com.danusin.search_danus.domain.persistence;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Table(name="vendor")
public class VendorModel implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Size(max=50)
    @Column(name="namaVendor", nullable= false)
    private String namaVendor;

    @Size(max=16)
    @Column(name="telepon",nullable = false)
    private String telepon;

    public long getId() {
        return id;
    }

    public String getNamaVendor() {
        return namaVendor;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setNamaVendor(String namaVendor) {
        this.namaVendor = namaVendor;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

}
