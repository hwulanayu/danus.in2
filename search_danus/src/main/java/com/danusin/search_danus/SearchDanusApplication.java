package com.danusin.search_danus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SearchDanusApplication {

	public static void main(String[] args) {
		SpringApplication.run(SearchDanusApplication.class, args);
	}

}
